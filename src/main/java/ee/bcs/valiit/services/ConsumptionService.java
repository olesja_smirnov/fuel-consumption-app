package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ee.bcs.valiit.model.ConsumptionStatistics;
import ee.bcs.valiit.model.Driver;
import ee.bcs.valiit.model.FuelConsumptionReport;
import ee.bcs.valiit.model.FuelData;
import ee.bcs.valiit.model.FuelType;

public class ConsumptionService {

	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/fuel_consumption";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	
	public static List<FuelData> getFuelConsumptionAllData() {
		List<FuelData> fuelDatas = new ArrayList<FuelData>();
		try {
			ResultSet result = executeSql("select fuel_data.*, driver.name as first_name, driver.surname as last_name from fuel_data "
					+ "inner join driver on driver.driver_code = fuel_data.driver_code order by fuel_data.id desc"); //sorteeritud, et salvestaks esimesena
			if (result != null) {
				while (result.next()) {
					FuelData fuelData = new FuelData();
					fuelData.setId(result.getInt("id"));
					fuelData.setDriverCode(result.getString("driver_code"));
					fuelData.setFirstName(result.getString("first_name"));
					fuelData.setLastName(result.getString("last_name"));
					
					fuelData.setFuelPrice(result.getDouble("fuel_price"));
					if (result.getDouble("volume_liters") > 0) {
						double tmpPricePerLiter = result.getDouble("fuel_price") / result.getDouble("volume_liters");
						tmpPricePerLiter = Math.round(tmpPricePerLiter * 100) / 100.0;
						fuelData.setPricePerLiter(tmpPricePerLiter);
					}
					fuelData.setVolumeLiters(result.getDouble("volume_liters"));
					fuelData.setPurchasingDate(result.getString("purchasing_date"));
					fuelData.setFuelType(result.getString("fuel_type"));
					fuelDatas.add(fuelData);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return fuelDatas;
	}

	
	public static FuelData getOneFuelData(int fuelDataId) {
		try {
			String sql = "select * from fuel_data WHERE id=" + fuelDataId;
			ResultSet result = executeSql(sql);
			if (result != null) {
				if (result.next()) {
					FuelData fuelData = new FuelData();
					fuelData.setId(result.getInt("id"));
					fuelData.setDriverCode(result.getString("driver_code"));
					fuelData.setFuelPrice(result.getDouble("fuel_price"));
					if (result.getDouble("volume_liters") > 0) {
						double tmpPricePerLiter = result.getDouble("fuel_price") / result.getDouble("volume_liters");
						tmpPricePerLiter = Math.round(tmpPricePerLiter * 100) / 100.0;
						fuelData.setPricePerLiter(tmpPricePerLiter);
					}
					fuelData.setVolumeLiters(result.getDouble("volume_liters"));
					fuelData.setPurchasingDate(result.getString("purchasing_date"));
					fuelData.setFuelType(result.getString("fuel_type"));
					return fuelData;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	
	public static void addFuelData(FuelData[] fuelData) {
		for (FuelData fuelDataElement : fuelData) {
			String sql = "INSERT INTO fuel_data (driver_code, fuel_price, volume_liters, purchasing_date, fuel_type) VALUES ('%s', '%s', '%s', '%s', '%s')";
			sql = String.format(sql, fuelDataElement.getDriverCode(), fuelDataElement.getFuelPrice(), fuelDataElement.getVolumeLiters(),
					fuelDataElement.getPurchasingDate(), fuelDataElement.getFuelType());
			executeSql(sql);
		}
		

	}

	// kontrollime fuelData driverCode, chtobi ne dublirovalis pri obnovlenii
	// servera

	public static boolean fuelDataNotExists(String driverCode) {
		return getFuelDataByDriverCode(driverCode) == null;
	}

	
	// don't need this method
	public static FuelData getFuelDataByDriverCode(String driverCode) {
		try {
			ResultSet result = executeSql(
					"select id, driver_code, fuel_price, volume_liters, purchasing_date, fuel_type from fuel_data where driverCode = '"
							+ driverCode + "'");
			if (result != null) {
				if (result.next()) {
					FuelData fuelData = new FuelData();
					fuelData.setId(result.getInt("id"));
					fuelData.setDriverCode(result.getString("driver_code"));
					fuelData.setFuelPrice(result.getDouble("fuel_price"));
					if (result.getDouble("volume_liters") > 0) {
						double tmpPricePerLiter = result.getDouble("fuel_price") / result.getDouble("volume_liters");
						tmpPricePerLiter = Math.round(tmpPricePerLiter * 100) / 100.0;
						fuelData.setPricePerLiter(tmpPricePerLiter);
					}
					fuelData.setVolumeLiters(result.getDouble("volume_liters"));
					fuelData.setPurchasingDate(result.getString("purchasing_date"));
					fuelData.setFuelType(result.getString("fuel_type"));
					return fuelData;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}


	// MODIFY Fuel Data
	public static void modifyFuelData(FuelData fuelData) {
		String sql = String.format(
				"UPDATE fuel_data SET driver_code = '%s', fuel_price = '%s', volume_liters = '%s', purchasing_date = '%s', fuel_type = '%s' WHERE id = '%s'",
				fuelData.getDriverCode(), fuelData.getFuelPrice(), fuelData.getVolumeLiters(), fuelData.getPurchasingDate(), 
				fuelData.getFuelType(), fuelData.getId());
		executeSql(sql);

	}

	// DELETE Fuel data
	public static void deleteFuelData(int fuelDataId) {
		String sql = "DELETE FROM fuel_data where id = " + fuelDataId;
		executeSql(sql);

	}

	//  1. Total spent amount of money grouped by month
	public static List<FuelConsumptionReport> getFuelConsumptionReports(int startYear, int startMonth, int monthCount, String driverCode) {
		List<FuelConsumptionReport> reports = new ArrayList<>();

		Calendar calendar = Calendar.getInstance();
		calendar.set(startYear, startMonth - 1, 1);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		for (int i = 0; i < monthCount; i++) {
			String tmpStartDate = dateFormat.format(calendar.getTime());
			calendar.add(Calendar.MONTH, 1);
			String tmpEndDate = dateFormat.format(calendar.getTime());
			reports.add(getFuelConsumptionReport(tmpStartDate, tmpEndDate, driverCode));
		}
		return reports;
	}

	//  1. Total spent amount of money grouped by month
	public static FuelConsumptionReport getFuelConsumptionReport(String fromDate, String toDate, String driverCode) {

		String sql = "";
		if (driverCode.length() > 0) { 		// tingimus pannakse selleks, kui raport on konkreetse driveri järgi või kõikide puhul.
			sql = String.format("SELECT * FROM fuel_data WHERE purchasing_date >= '%s' AND purchasing_date < '%s' AND driver_code = '%s'",
			fromDate, toDate, driverCode);
		} else {
			sql = String.format("SELECT * FROM fuel_data WHERE purchasing_date >= '%s' AND purchasing_date < '%s'",
					fromDate, toDate);
	}
		ResultSet consumptionSet = executeSql(sql);

		
		if (consumptionSet != null) {
			// "2018-03-01" "2018-04-01"
			String[] fromDateParts = fromDate.split("-");

			FuelConsumptionReport report = new FuelConsumptionReport();
			report.setYear(Integer.parseInt(fromDateParts[0]));
			report.setMonth(Integer.parseInt(fromDateParts[1]));

			double totalCost = 0;

			// liidame kokku kõigi kuluaruannete total costid.
			try {
				while (consumptionSet.next()) {
					totalCost = totalCost + consumptionSet.getDouble("fuel_price");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			totalCost = Math.round(totalCost * 100) / 100.0;

			report.setTotalCost(totalCost);
			return report;
		}

		return new FuelConsumptionReport();
	}

	//  2. List fuel consumption records for specified month
	public static List<FuelData> getFuelDataAllByMonth(int startYear, int startMonth, String driverCode) {
		List<FuelData> allDataReport = new ArrayList<FuelData>();

		Calendar calendar = Calendar.getInstance();
		calendar.set(startYear, startMonth - 1, 1);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String fromDate = dateFormat.format(calendar.getTime());
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.DATE, 1);
		String toDate = dateFormat.format(calendar.getTime());
		


		try {
			String sql = "";
			if (driverCode.length() > 0) { 		// tingimus pannakse selleks, kui raport on konkreetse driveri järgi või kõikide puhul.
				sql = String.format("SELECT * FROM fuel_data WHERE purchasing_date >= '%s' AND purchasing_date < '%s' AND driver_code = '%s'",
				fromDate, toDate, driverCode);
			} else {
				sql = String.format("SELECT * FROM fuel_data WHERE purchasing_date >= '%s' AND purchasing_date < '%s'",
						fromDate, toDate);
		}
			ResultSet result = executeSql(sql);
			
			if (result != null) {
				while (result.next()) {
					FuelData fuelData = new FuelData();
					fuelData.setId(result.getInt("id"));
					fuelData.setDriverCode(result.getString("driver_code"));
					fuelData.setFuelPrice(result.getDouble("fuel_price"));
					if (result.getDouble("volume_liters") > 0) {
						double tmpPricePerLiter = result.getDouble("fuel_price") / result.getDouble("volume_liters");
						tmpPricePerLiter = Math.round(tmpPricePerLiter * 100) / 100.0;
						fuelData.setPricePerLiter(tmpPricePerLiter);
					}
					fuelData.setVolumeLiters(result.getDouble("volume_liters"));
					fuelData.setPurchasingDate(result.getString("purchasing_date"));
					fuelData.setFuelType(result.getString("fuel_type"));
					allDataReport.add(fuelData);
				}
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allDataReport;
	}

	
	// 3.Statistic for each month (grouped by Fuel Type).
	public static List<ConsumptionStatistics> getConsumptionStatisticsByMonths(int startYear, int startMonth, int monthCount, String fuelType, String driverCode) {

		List<ConsumptionStatistics> reports = new ArrayList<>();

		Calendar calendar = Calendar.getInstance();
		calendar.set(startYear, startMonth - 1, 1);

		for (int i = 0; i < monthCount; i++) {
			int tmpYear = calendar.get(Calendar.YEAR);
			int tmpMonth = calendar.get(Calendar.MONTH) + 1;
			reports.add(getConsumptionStatisticsForPeriod(tmpYear, tmpMonth, 1, fuelType, driverCode));
			calendar.add(Calendar.MONTH, 1);
		}

		return reports;
	}

	// 3. Statistics for each month (grouped by Fuel Type).
	private static ConsumptionStatistics getConsumptionStatisticsForPeriod(int startYear, int startMonth,
			int monthCount, String fuelType, String driverCode) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.set(startYear, startMonth - 1, 1);
		String fromDate = dateFormat.format(calendar.getTime());

		calendar.add(Calendar.MONTH, monthCount);
		String toDate = dateFormat.format(calendar.getTime());

		String sql = "";
		
		if (driverCode.length() > 0) { 		// tingimus pannakse selleks, kas raport on konkreetse driveri järgi või kõikide puhul.
			sql = String.format("SELECT * FROM fuel_data WHERE purchasing_date >= '%s' AND purchasing_date < '%s' AND fuel_type = '%s' AND driver_code = '%s'",
				fromDate, toDate, fuelType, driverCode);
		} else {
			sql = String.format("SELECT * FROM fuel_data WHERE purchasing_date >= '%s' AND purchasing_date < '%s' AND fuel_type = '%s'",
					fromDate, toDate, fuelType);
		}

		ResultSet consumptionSet = executeSql(sql);

		ConsumptionStatistics report = new ConsumptionStatistics();

		if (consumptionSet != null) {

			String[] fromDateParts = fromDate.split("-");

			report.setYear(Integer.parseInt(fromDateParts[0]));
			report.setMonth(Integer.parseInt(fromDateParts[1]));
			report.setFuelType(fuelType);
			
			double totalCost = 0;
			double totalVolume = 0;
			double averagePrice = 0;


			try {
				while (consumptionSet.next()) {
					totalCost = totalCost + consumptionSet.getDouble("fuel_price");
					totalVolume = totalVolume + consumptionSet.getDouble("volume_liters");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			averagePrice = totalCost / totalVolume;

			totalCost = Math.round(totalCost * 100) / 100.0;
			report.setTotalCost(totalCost);
			totalVolume = Math.round(totalVolume * 100) / 100.0;
			report.setTotalVolume(totalVolume);
			averagePrice = Math.round(averagePrice * 100) / 100.0;
			report.setAveragePrice(averagePrice);

		}

		return report;
	}

	
	// 3. Lisame konstantid Fuel Type, kutsume neid SQL-ist välja
	public static List<String> getFuelTypes() {
		List<String> fuelTypes = new ArrayList<>();
		
		try {
			ResultSet result = executeSql("select * from fuel_type");
			if (result != null) {
				while (result.next()) {
					fuelTypes.add(result.getString("fuel_type"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return fuelTypes;
	}

	

	
	// kontrollime nuppu EDIT, et uus rida ei olnud lisatud peale andmete parandamist
	public static FuelData getFuelDataByFuelType(String driverCode) {
		 try {
		 ResultSet result = executeSql("select id, driver_code, fuel_price, volume_liters, purchasing_date,"
		 		+ "fuel_type from fuel_consumption where driver_code = '" + driverCode + "'");
		 if (result != null) {
			 if (result.next()) {
				 FuelData fuelData = new FuelData();
				 fuelData.setId(result.getInt("id"));
				 fuelData.setDriverCode(result.getString("driver_code"));
				 fuelData.setFuelPrice(result.getDouble("fuel_price"));
				 fuelData.setVolumeLiters(result.getDouble("volume_liters"));
				 fuelData.setPurchasingDate(result.getString("purchasing_date"));
				 fuelData.setFuelType(result.getString("fuel_type"));
			 }
		 }
		 } catch (SQLException e) {
		 e.printStackTrace();
		 }

		 return null;
	}
		 
		 
		 public static List<Driver> getDrivers() {
			 List<Driver> drivers = new ArrayList<Driver>();
				try {
					ResultSet result = executeSql("select * from driver");
					if (result != null) {
						while (result.next()) {
							Driver driver = new Driver();
							driver.setDriverCode(result.getString("driver_code"));
							driver.setName(result.getString("name"));
							driver.setSurname(result.getString("surname"));
							drivers.add(driver);
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return drivers;
			}
			
		 
		 // 	Fuel Type väljakutsumine SQL-ist
			public static List<FuelType> getFuelTypeMenu() {
				List<FuelType> fuelTypes = new ArrayList<FuelType>();
				try {
					ResultSet result = executeSql("select * from fuel_type");
					if (result != null) {
						while (result.next()) {
							FuelType type = new FuelType();
							type.setFuelType(result.getString("fuel_type"));
							type.setName(result.getString("name"));
							fuelTypes.add(type);
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return fuelTypes;
			}


			// adding opportunity to insert new Driver to the database
			public static void addNewDriver(Driver[] drivers) {
				for (Driver driver : drivers) {
					String sql = "INSERT INTO driver (driver_code, name, surname) VALUES ('%s', '%s', '%s')";
					sql = String.format(sql, driver.getDriverCode(), driver.getName(), driver.getSurname());
					executeSql(sql);	
				}
			}	
			
			
}
	
	




 
